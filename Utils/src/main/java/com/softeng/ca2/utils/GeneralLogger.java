package com.softeng.ca2.utils;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.MessageFormat;
import java.util.Date;
import java.util.logging.*;

/**
 * Created by Amir Vakili on 11/17/2015.
 */
public class GeneralLogger {
    private Logger logger;

    public GeneralLogger(String s) {
        logger = Logger.getLogger(s);
    }

    public void log(Level level, String s) {
        logger.log(level, s);
    }

    public void addHandler(String fileName) throws IOException {
        FileHandler fileHandler = new FileHandler(fileName);
        fileHandler.setFormatter(new Formatter() {
            Date dat = new Date();
            private final static String format = "{0,date} {0,time}";
            private MessageFormat formatter;
            private Object args[] = new Object[1];

            // Line separator string.  This is the value of the line.separator
            // property at the moment that the SimpleFormatter was created.
            //private String lineSeparator = (String) java.security.AccessController.doPrivileged(
            //        new sun.security.action.GetPropertyAction("line.separator"));
            private String lineSeparator = "\n";
            @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
            @Override
            synchronized public String format(LogRecord record){
                StringBuilder sb = new StringBuilder();

                // Minimize memory allocations here.
                dat.setTime(record.getMillis());
                args[0] = dat;


                // Date and time
                StringBuffer text = new StringBuffer();
                if (formatter == null) {
                    formatter = new MessageFormat(format);
                }
                formatter.format(args, text, null);
                sb.append(text);
                sb.append(" ");



                sb.append(lineSeparator);
                String message = formatMessage(record);

                // Level
                sb.append(record.getLevel().getLocalizedName());
                sb.append(": ");

                // Indent - the more serious, the more indented.
                //sb.append( String.format("% ""s") );
                int iOffset = (1000 - record.getLevel().intValue()) / 100;
                for( int i = 0; i < iOffset;  i++ ){
                    sb.append(" ");
                }


                sb.append(message);
                sb.append(lineSeparator);
                if (record.getThrown() != null) {
                    try {
                        StringWriter sw = new StringWriter();
                        PrintWriter pw = new PrintWriter(sw);
                        record.getThrown().printStackTrace(pw);
                        pw.close();
                        sb.append(sw.toString());
                    } catch (Exception ignored) {
                    }
                }
                return sb.toString();
            }
        });
        logger.addHandler(fileHandler);
        this.log(Level.CONFIG, "Added new file handler " + fileName);
    }



}
