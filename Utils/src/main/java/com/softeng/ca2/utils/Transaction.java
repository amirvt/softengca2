package com.softeng.ca2.utils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Locale;

/**
 * Created by Amir Vakili on 11/3/2015.
 */
public class Transaction implements Serializable{
    private String id;
    private final Type type;
    private final BigDecimal amount;
    private final String deposit;

    private static DecimalFormat decimalFormat;
    static {
        decimalFormat = new DecimalFormat("#,###", new DecimalFormatSymbols(Locale.ENGLISH));
        decimalFormat.setParseBigDecimal(true);
    }

    public Transaction(String id, Type type, String amount, String deposit) throws ParseException {

        this.id = id;
        this.type = type;
        ParsePosition pos = null;
        this.amount = parseBigDecimalStrict(amount);
        this.deposit = deposit;
    }

    private static BigDecimal parseBigDecimalStrict(String aValue) throws ParseException {
        ParsePosition parsePosition = new ParsePosition(0);
        BigDecimal result = (BigDecimal) decimalFormat.parse(aValue, parsePosition);

        if(result == null) {
            throw new ParseException(aValue, parsePosition.getIndex());
        }
        else if(parsePosition.getIndex() < aValue.length()) {
            throw new ParseException(aValue, parsePosition.getIndex());
        }
        return result;
    }


    public String getId() {
        return id;
    }

    public Type getType() {
        return type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getDeposit() {
        return deposit;
    }



    public enum Status {
        OK, INVALID_ACCOUNT, INSUFFICIENT_FUNDS, UPPERBOUND_REACHED
    }

    public enum Type {
        WITHDRAW, DEPOSIT
    }
}

