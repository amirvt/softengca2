package com.softeng.ca2.utils;

import java.util.function.Consumer;

/**
 * Created by Amir Vakili on 11/9/2015.
 */
public class Action<T> implements Runnable {
    private T param;
    private Consumer<T> consumer;

    public Action(T param, Consumer<T> consumer) {
        this.param = param;
        this.consumer = consumer;
    }

    @Override
    public void run() {
        consumer.accept(param);
    }
}
