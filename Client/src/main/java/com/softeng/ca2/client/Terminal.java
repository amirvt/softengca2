package com.softeng.ca2.client;

import com.softeng.ca2.utils.GeneralLogger;
import com.softeng.ca2.utils.Transaction;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;

/**
 * Created by Amir Vakili on 11/3/2015.
 */
public class Terminal {
    GeneralLogger generalLogger;
    private String serverIp;
    private int serverPort;
    private List<Transaction> transactions = new ArrayList<>();
    private String id;
    private String terminalType;
    public String outLog;


    public void writeResponses(List<Response> responses) {
        final XMLOutputFactory factory = XMLOutputFactory.newInstance();

        try {
            final XMLStreamWriter writer = factory.createXMLStreamWriter(
                    new FileWriter("response" + id  + ".xml"));

            writer.writeStartDocument();
            writer.writeStartElement("responses");
            for (Response response : responses) {
                writer.writeEmptyElement("response");
                writer.writeAttribute("transactionId", response.getTransactionId());
                writer.writeAttribute("status", response.getStatus().toString());
            }
            writer.writeEndElement();
            writer.writeEndDocument();

            writer.flush();
            writer.close();

        } catch (XMLStreamException | IOException e) {
            System.err.println("Error writing to response.xml");
            System.exit(-1);
        }
    }

    public void loadConfiguration(String transactionsFileName) {
        try {
            parseConfigFile(transactionsFileName);
        } catch (FileNotFoundException e) {
            generalLogger.log(Level.SEVERE, "transactions xml file not found\n" + e.getMessage());
            System.exit(-1);
        } catch (IOException e) {
            generalLogger.log(Level.SEVERE, "Error accessing transactions xml file\n" + e.getMessage());
        } catch (SAXException | ParserConfigurationException e) {
            generalLogger.log(Level.SEVERE, "Error Parsing %s\n" + transactionsFileName + "\n" + e.getMessage());
            System.exit(-1);
        }
    }

    private void parseConfigFile(String transactionsFileName) throws ParserConfigurationException, SAXException, IOException {
        SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
        File transactionsFile = new File(transactionsFileName);
        validateTransactionsFile(transactionsFile);
        parser.parse(
                transactionsFile,
                new ClientSAXHandler(this));
    }

    private void validateTransactionsFile(File transactionsFile) {
        try {
            validateWithXSD(transactionsFile);
        } catch (SAXException e) {
            throw new RuntimeException("transactions.xsd has invalid xml");
        } catch (IOException e) {
            generalLogger.log(Level.SEVERE, "Unexpected error while validating " + transactionsFile + "\n" + e.getMessage());
            System.exit(-1);
        }
    }

    private void validateWithXSD(File transactionsFile) throws SAXException, IOException {
        InputStream xsd = new FileInputStream("transactions.xsd");
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = factory.newSchema(new StreamSource(xsd));
        Validator validator = schema.newValidator();

        final List<SAXParseException> exceptions = new LinkedList<>();
        validator.setErrorHandler(new ErrorHandler() {
            @Override
            public void warning(SAXParseException exception) throws SAXException {
                exceptions.add(exception);
            }

            @Override
            public void fatalError(SAXParseException exception) throws SAXException {
                exceptions.add(exception);
            }

            @Override
            public void error(SAXParseException exception) throws SAXException {
                exceptions.add(exception);
            }
        });

        StreamSource xmlFile = new StreamSource(transactionsFile);
        validator.validate(xmlFile);
        if (!exceptions.isEmpty()) {
            System.err.println(transactionsFile.getName() + "was incorrectly formatted.");
            exceptions.forEach(e -> System.err.println(e.getMessage()));
            System.exit(-1);
        }
    }

    public String getServerIp() {
        return serverIp;
    }

    public int getServerPort() {
        return serverPort;
    }

    void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }

    void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    void addTransaction(Transaction transaction) {
        transactions.add(transaction);
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setTerminalType(String terminalType) {
        this.terminalType = terminalType;
    }

    public String getTerminalType() {
        return terminalType;
    }
}
