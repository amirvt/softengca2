package com.softeng.ca2.client;

import com.softeng.ca2.utils.Transaction;

/**
 * Created by Amir Vakili on 11/17/2015.
 */
public class Response {
    String transactionId;
    Transaction.Status status;

    public Response(String transactionId, Transaction.Status status) {
        this.transactionId = transactionId;
        this.status = status;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public Transaction.Status getStatus() {
        return status;
    }
}
