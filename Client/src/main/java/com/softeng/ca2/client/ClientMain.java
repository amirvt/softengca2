package com.softeng.ca2.client;

/**
 * Created by Amir Vakili on 11/3/2015.
 */
public class ClientMain {
    public static void main(String[] args) {
        if ( args.length < 1 ) {
            System.err.println("transactions xml file not provided");
            System.exit(-1);
        }
        Client client = new Client(args[0]);
        client.ConnectAndSendTransactions();
    }
}
