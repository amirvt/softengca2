package com.softeng.ca2.client;

import com.softeng.ca2.utils.GeneralLogger;
import com.softeng.ca2.utils.Transaction;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.logging.Level;

/**
 * Created by Amir Vakili on 11/17/2015.
 */
public class ServerConnection {
    Socket socket;
    ObjectOutputStream oos;
    ObjectInputStream ois;
    private GeneralLogger generalLogger;

    public ServerConnection(String serverIp, int serverPort, GeneralLogger generalLogger) {
        this.generalLogger = generalLogger;
        socket = new Socket();
        connectToServer(serverIp, serverPort);
        wrapInputOutputStream();
    }

    void sendId(String id) {
        try {
            oos.writeObject(id);
        } catch (IOException e) {
            generalLogger.log(Level.SEVERE, "Connection could not be established, " + e.getMessage());
        }
    }

    private void wrapInputOutputStream() {
        try {
            oos = new ObjectOutputStream(socket.getOutputStream());
            ois = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            generalLogger.log(Level.SEVERE, e.getMessage());
        }
    }

    private void connectToServer(String serverIp, int serverPort) {
        try {
            socket.connect(new InetSocketAddress(serverIp, serverPort));
        } catch (IOException e) {
            generalLogger.log(Level.SEVERE, "Could not connect to server\n" + e.getMessage());
            System.err.println("Server not listening");
            System.exit(-1);
        }
    }

    Transaction.Status sendTransaction(Transaction transaction) throws IOException {
        oos.writeObject(transaction);
        return receiveResponse();
    }

    private Transaction.Status receiveResponse() throws IOException {
        Transaction.Status responseStatus;
        try {
            responseStatus = (Transaction.Status) ois.readObject();
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Server didn't send Transaction.Status enum as response");
        }
        return responseStatus;
    }

    public void closeConnection() {
        try {
            oos.writeObject(null);
            socket.close();
        } catch (IOException e) {

            generalLogger.log(Level.WARNING, "Failed to close connection to server, " +e.getMessage());
        }
    }
}
