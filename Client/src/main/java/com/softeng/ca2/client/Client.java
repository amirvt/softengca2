package com.softeng.ca2.client;

import com.softeng.ca2.utils.GeneralLogger;
import com.softeng.ca2.utils.Transaction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Created by Amir Vakili on 11/17/2015.
 */
public class Client {
    private List<Response> responses = new ArrayList<>();

    private ServerConnection myServerConnection;
    private Terminal terminal;

    private GeneralLogger generalLogger;

    public Client(String transactionsFile) {
        terminal = new Terminal();
        terminal.loadConfiguration(transactionsFile);
        generalLogger = new GeneralLogger(terminal.getId());
        try {
            generalLogger.addHandler(terminal.outLog);
        } catch (IOException e) {
            generalLogger.log(Level.SEVERE, "Cannot access specified log file: " + transactionsFile);
        }
    }

    public void ConnectAndSendTransactions() {
        myServerConnection = new ServerConnection(terminal.getServerIp(), terminal.getServerPort(), generalLogger);
        myServerConnection.sendId(terminal.getId());
        sendTransactions();
        myServerConnection.closeConnection();
        terminal.writeResponses(responses);
    }

    private void sendTransactions() {
        try {
            for (Transaction transaction : terminal.getTransactions()) {
                Transaction.Status responseStatus =
                        myServerConnection.sendTransaction(transaction);
                responses.add(new Response(transaction.getId(), responseStatus));
                logTransaction(transaction, responseStatus);
            }
        } catch (IOException e) {
            generalLogger.log(Level.SEVERE, "Cannot send or receive messages to and from the server due to" + e.getMessage());
            System.exit(-1);
        }
    }

    public void logTransaction(Transaction transaction, Transaction.Status status) {
        switch (status) {
            case OK:
                generalLogger.log(Level.INFO, "transaction of id " + transaction.getId() + " completed successfully");
                break;
            case INSUFFICIENT_FUNDS:
                generalLogger.log(Level.INFO, "transaction of id " + transaction.getId() + " failed due to insufficient funds");
                break;
            case INVALID_ACCOUNT:
                generalLogger.log(Level.INFO, "transaction of id " + transaction.getId() + " failed due to referencing an invalid account");
                break;
            case UPPERBOUND_REACHED:
                generalLogger.log(Level.INFO, "transaction of id " + transaction.getId() + " failed due to having breached the accounts upper bound");
                break;
        }
    }

}
