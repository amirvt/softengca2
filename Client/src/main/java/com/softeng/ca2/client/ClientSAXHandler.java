package com.softeng.ca2.client;

import com.softeng.ca2.utils.Transaction;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.text.ParseException;
import java.util.Locale;

/**
 * Created by Amir Vakili on 11/17/2015.
 */
class ClientSAXHandler extends DefaultHandler {

    private Terminal terminal;

    public ClientSAXHandler(Terminal terminal) {
        this.terminal = terminal;
    }

    @Override
    public void startElement(String s, String s1, String s2, Attributes attributes) throws SAXException {
        switch (s2) {
            case "terminal":
                setIdAndType(attributes);
                break;
            case "server":
                setIpAndPort(attributes);
                break;
            case "outLog":
                setOutLog(attributes);
                break;
            case "transaction":
                CreateAndAddTransaction(attributes);
                break;
        }
    }

    private void setIdAndType(Attributes attributes) {
        terminal.setId(attributes.getValue("id"));
        terminal.setTerminalType(attributes.getValue("type"));
    }

    private void CreateAndAddTransaction(Attributes attributes) {
        try {
            Transaction transaction = (new Transaction(
                    attributes.getValue("id"),
                    Transaction.Type.valueOf(attributes.getValue("type").toUpperCase(Locale.ENGLISH)),
                    attributes.getValue("amount"),
                    attributes.getValue("deposit")
            ));
            terminal.addTransaction(transaction);
        } catch (ParseException e) {
            System.err.println("Amount" + attributes.getValue("amount") + " for transaction of id" + attributes.getValue("id") + " has an invalid number for its amount. Skipping...");
        }
    }

    private void setOutLog(Attributes attributes) {
        try {
            terminal.outLog = (attributes.getValue("path"));
        } catch (NullPointerException e) {
            System.err.println("Missing path attribute");
        }
    }

    private void setIpAndPort(Attributes attributes) {
        terminal.setServerIp(attributes.getValue("ip"));
        terminal.setServerPort(Integer.parseInt(attributes.getValue("port")));
    }
}
