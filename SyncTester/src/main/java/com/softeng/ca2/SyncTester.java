package com.softeng.ca2;

import com.softeng.ca2.client.Client;
import com.softeng.ca2.server.Server;
import com.softeng.ca2.utils.Action;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Hello world!
 *
 */
public class SyncTester
{
    public static void main( String[] args ) throws InterruptedException {
        Server server = new Server();

        Action<Server> serverRunnable = new Action<>(server, Server::start);
        Thread serverThread = new Thread(serverRunnable);
        serverThread.start();

        ExecutorService executorService = Executors.newFixedThreadPool(5);

        for (int i = 1; i <= 5; i++) {
            Action<Client> clientRunnable = new Action<>(new Client("transactions" + i + ".xml"), Client::ConnectAndSendTransactions);
            executorService.submit(clientRunnable);
        }


        serverThread.join();

    }


}
