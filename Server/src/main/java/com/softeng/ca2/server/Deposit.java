package com.softeng.ca2.server;

import com.softeng.ca2.utils.Transaction;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;

//import javax.annotation.

/**
 * Created by Amir Vakili on 11/5/2015.
 */
public class Deposit {
    private final String customer;
    private final String id;
    private BigDecimal balance;
    private final BigDecimal upperBound;

    private static DecimalFormat decimalFormat;
    static {
        decimalFormat = new DecimalFormat("#,###", new DecimalFormatSymbols(Locale.ENGLISH));
        decimalFormat.setParseBigDecimal(true);
    }

    public Deposit(String customer, String id, String initialBalance, String upperBound) throws ParseException {

        this.balance = (BigDecimal) decimalFormat.parse(initialBalance);
        this.upperBound = (BigDecimal) decimalFormat.parse(upperBound);

        this.id = id;
        this.customer = customer;
    }

     synchronized public Transaction.Status updateBalance(Transaction transaction) {
        switch (transaction.getType()) {
            case DEPOSIT:
                return depositAmount(transaction.getAmount());
            case WITHDRAW:
               return withdrawAmount(transaction.getAmount());
        }
        throw new RuntimeException("Unhandled Exception Type: " + transaction.getType());
    }

    private Transaction.Status depositAmount(BigDecimal amount){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        BigDecimal newBalance = balance.add(amount);
        if (newBalance.compareTo(upperBound) > 0) {
            return Transaction.Status.UPPERBOUND_REACHED;
        } else {
            balance = newBalance;
            return Transaction.Status.OK;

        }
    }

    private Transaction.Status withdrawAmount(BigDecimal amount) {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        BigDecimal newBalance = balance.subtract(amount);
        if (newBalance.compareTo(BigDecimal.ZERO) < 0) {
            return Transaction.Status.INSUFFICIENT_FUNDS;
        } else {
            balance = newBalance;
            return Transaction.Status.OK;
        }
    }


    public String getId() {
        return id;
    }

    public String getCustomer() {
        return customer;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public BigDecimal getUpperBound() {
        return upperBound;
    }


}
