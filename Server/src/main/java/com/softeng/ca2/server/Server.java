package com.softeng.ca2.server;

import com.softeng.ca2.utils.Action;
import com.softeng.ca2.utils.GeneralLogger;
import com.softeng.ca2.utils.Transaction;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;

/**
 * Created by Amir Vakili on 11/17/2015.
 */
public class Server {
    private static ExecutorService executorService = Executors.newCachedThreadPool();
    private Bank bank;
    private GeneralLogger generalLogger = new GeneralLogger("Server");

    public Server() {
        bank = Bank.loadBank(generalLogger);
    }

    public void start() {

        WatchInputAsync();
        ServerSocket serverSocket = CreateServerSocket();

        //noinspection InfiniteLoopStatement
        while (true) {
            Socket socket;
            try {
                socket = serverSocket.accept();
                executorService.submit(new Action<>(socket, this::handleClientConnection));
            } catch (IOException e) {
                generalLogger.log(Level.WARNING, "Failed to establish a connection due to " + e);
            }
        }


    }

    private void WatchInputAsync() {
        new Thread(this::InputHandler).start();
    }

    private ServerSocket CreateServerSocket() {
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket();
            serverSocket.setReuseAddress(true);
            serverSocket.bind(new InetSocketAddress(bank.getPort()));
        } catch (IOException e) {
            generalLogger.log(Level.SEVERE, e.getMessage());
            System.err.println("Failed to initialize Server socket");
            System.exit(-1);
        }
        return serverSocket;
    }

    private void InputHandler() {
        BufferedReader bi = new BufferedReader(new InputStreamReader(System.in));
        String line;

        try {
            while ((line = bi.readLine()) != null) {
                if (Objects.equals(line, "sync")) {
                    bank.writeDepositsToJson();
                } else if (Objects.equals(line, "quit")) {
                    System.exit(0);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("An Error occurred when reading from standard input");
        }

    }

    private void handleClientConnection(Socket socket) {

        try {
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());

            String id = (String) ois.readObject();

            while (true) {
                Transaction transaction = (Transaction) ois.readObject();
                if (transaction == null)
                    break;
                Transaction.Status result = bank.applyTransaction(transaction, id);
                oos.writeObject(result);
            }
            socket.close();
        } catch (IOException e) {
            generalLogger.log(Level.WARNING, "Connection to " + socket.getRemoteSocketAddress() + " terminated due to\n " + e.getMessage());
        } catch (ClassNotFoundException e) {
            generalLogger.log(Level.WARNING, "Connection to " + socket.getRemoteSocketAddress() + " sent invalid data");
        }


    }
}
