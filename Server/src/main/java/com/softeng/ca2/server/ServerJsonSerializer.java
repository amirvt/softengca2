package com.softeng.ca2.server;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Amir Vakili on 11/5/2015.
 */
public class ServerJsonSerializer implements JsonSerializer<Bank> {
    @Override
    public JsonElement serialize(Bank bank, Type type, JsonSerializationContext jsonSerializationContext) {
        final JsonObject jServer = new JsonObject();
        jServer.addProperty("port", bank.getPort());

        final JsonArray jDepositsArray = new JsonArray();
        bank.getDepositsMap().values().forEach(deposit -> {
            JsonObject jDeposit = CreateDepositJsonObject(deposit);
            jDepositsArray.add(jDeposit);
        });


//        bank.getDepositsMap().values().stream().map(this::CreateDepositJsonObject);
//        JsonArray j = JsonArray.

        jServer.add("deposits", jDepositsArray);

        jServer.addProperty("outLog", bank.getOutLog());

        return jServer;
    }

    private JsonObject CreateDepositJsonObject(Deposit deposit) {
        JsonObject jDeposit = new JsonObject();
        jDeposit.addProperty("customer", deposit.getCustomer());
        jDeposit.addProperty("id", deposit.getId());
        jDeposit.addProperty("initialBalance", NumberFormat.getNumberInstance(Locale.ENGLISH).format(deposit.getBalance()));
        jDeposit.addProperty("upperBound", NumberFormat.getNumberInstance(Locale.ENGLISH).format(deposit.getUpperBound()));
        return jDeposit;
    }
}
