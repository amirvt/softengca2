package com.softeng.ca2.server;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Amir Vakili on 11/8/2015.
 */
public class ServerJsonDeserializer implements JsonDeserializer<Bank> {
    @Override
    public Bank deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {

        Integer port = ParsePortNumber(jsonElement);
        String outLog = setUpLogFile(jsonElement);
        Map<String, Deposit> depositMap = parseDeposits(jsonElement);

        return new Bank(port,outLog, depositMap);
    }

    private Map<String, Deposit> parseDeposits(JsonElement jsonElement) {
        Map<String, Deposit> depositMap = new HashMap<>();
        jsonElement.getAsJsonObject().get("deposits").getAsJsonArray().forEach(jDeposit -> {
            Deposit deposit = null;
            try {
                deposit = new Deposit(
                        jDeposit.getAsJsonObject().get("customer").getAsString(),
                        jDeposit.getAsJsonObject().get("id").getAsString(),
                        jDeposit.getAsJsonObject().get("initialBalance").getAsString(),
                        jDeposit.getAsJsonObject().get("upperBound").getAsString()
                );
                depositMap.put(deposit.getId(), deposit);
            } catch (ParseException | NullPointerException e) {
                System.err.println("One of the Deposits in core.json is incorrectly formatted\n" + e.getMessage());
            }

        });
        return depositMap;
    }

    private Integer ParsePortNumber(JsonElement jsonElement) {
        Integer port = null;
        try {
            port = jsonElement.getAsJsonObject().get("port").getAsInt();
        } catch (NullPointerException e) {
            System.err.println("core.json file has no port.");
            System.exit(-1);
        }
        return port;
    }

    private String setUpLogFile(JsonElement jsonElement) {
        try {
            return jsonElement.getAsJsonObject().get("outLog").getAsString();
        } catch (NullPointerException e) {
            System.err.println("core.json file has no outLog.\n" + e.getMessage());
        }
        throw new RuntimeException();
    }
}
