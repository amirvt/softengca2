package com.softeng.ca2.server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.softeng.ca2.utils.GeneralLogger;
import com.softeng.ca2.utils.Transaction;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;


/**
 * Created by Amir Vakili on 11/4/2015.
 */
public class Bank {
    private static Gson gson;
    private int port;
    private GeneralLogger generalLogger;
    private Map<String, Deposit> depositMap = new HashMap<>();

    static {
        setupJsonSerializer();
    }

    private String outLog;

    private static void setupJsonSerializer() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Bank.class, new ServerJsonSerializer());
        gsonBuilder.registerTypeAdapter(Bank.class, new ServerJsonDeserializer());
        gsonBuilder.setPrettyPrinting();
        gson = gsonBuilder.create();
    }

    public static Bank loadBank(GeneralLogger generalLogger) {
        System.err.println("Deserializing core.json ...");
        Bank bank = null;
        try (BufferedReader bf = new BufferedReader(new FileReader("core.json"))) {
            bank = gson.fromJson(bf, Bank.class);
            bank.setLogger(generalLogger);
        } catch (FileNotFoundException e) {
            System.err.println("core.json file not found");
            System.exit(-1);
        } catch (IOException e) {
            System.err.println("Could not read core.json file");
            System.err.println(e.getMessage());
            System.exit(-1);
        }
        System.err.println("Serialization successful");
        return bank;
    }

    Bank(int port, String outLog, Map<String, Deposit> depositMap) {
        this.port = port;
        this.depositMap = depositMap;
        this.outLog = outLog;
    }

    public int getPort() {
        return port;
    }

    public Map<String, Deposit> getDepositsMap() {
        return depositMap;
    }

    void writeDepositsToJson() {
        System.out.println("Serializing to core.json ...");
        try (FileWriter fileWriter = new FileWriter("core.json")) {
            gson.toJson(this, fileWriter);
        } catch (IOException e) {
            generalLogger.log(Level.WARNING, "Could not serialize json\n" + e.getMessage());
        }
        System.out.println("Serialization successful");
    }

    Transaction.Status applyTransaction(Transaction transaction, String terminalId) {
        Deposit deposit = depositMap.get(transaction.getDeposit());
        Transaction.Status status;
        if (deposit == null)
            status = Transaction.Status.INVALID_ACCOUNT;
        else
            status = deposit.updateBalance(transaction);
        generalLogger.log(Level.INFO, "transaction of id " + transaction.getId() + " from terminal " + terminalId + "completed with status of " + status +
                (status == Transaction.Status.OK ? ("\n        Deposit " + transaction.getDeposit() + " now has an amount of " + deposit.getBalance()) : ""));
        return status;
    }


    public String getOutLog() {
        return outLog;
    }

    public void setOutLog(String outLog) {
        this.outLog = outLog;
    }

    public void setLogger(GeneralLogger logger) {
        this.generalLogger = logger;
        try {
            generalLogger.addHandler(outLog);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


