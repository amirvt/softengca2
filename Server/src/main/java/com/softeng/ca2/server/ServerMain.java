package com.softeng.ca2.server;

/**
 * Created by Amir Vakili on 11/3/2015.
 */
public class ServerMain {
    public static void main(String[] args) {
        Server server = new Server();
        server.start();
    }
}
